from django.db import models
import django.utils.timezone
from datetime import datetime 

# Create your models here.
class StatusModel(models.Model):
	status = models.TextField(max_length=300)
	date = models.DateTimeField(default=datetime.now, blank=True)